
<!DOCTYPE html>
<html lang="fr">

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <link rel="stylesheet" href="style.css" />
    <title>Carte Dominique Lacouture</title>
</head>

<body>
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div id="menu" class="col-3 p-0">
                <div class="row align-items-center h-25 p-1 bg-dark text-white">
                    <div class="container">
                        <h1 class="text-center font-weight-bold">Dominique Lacouture</h1>
                        <p class="text-center font-italic">Master 2 Siglis - Web Mapping<br>Contact : <a href="mailto:domlac47@gmail.com" class="font-italic text-white"><u>domlac47@gmail.com</u></a></p><!--Adressez vous au Roi !-->
                    </div>
                </div>
                <div class="row align-items-center h-75 selection">
                    <div class="container">
                        <div class="container p-2 m-0">
                            <h4 class="text-dark font-weight-bold p-1">Cercles proportionnels</h4>
                            <div class="input-group p-0">
                                <a class="col-8 list-group-item list-group-item-action indicateur" data-id="1" data-type="cercle" data-radius="50000" href="#">Population municipale</a>
                                <select name="anneePopMuni" data-id="1" class="col-4">
                                    <option value="" selected>Année</option>
                                    <option value="d68">1968</option>
                                    <option value="d75">1975</option>
                                    <option value="d82">1982</option>
                                    <option value="d90">1990</option>
                                    <option value="d99">1999</option>
                                    <option value="p06">2006</option>
                                    <option value="p11">2011</option>
                                    <option value="p16">2016</option>
                                </select>
                            </div>
                            <div class="input-group p-0">
                                <a class="col-12 list-group-item list-group-item-action indicateur" data-id="2" data-type="cercle" data-radius="500" href="#">Superficie</a>
                            </div>
                            <div class="input-group p-0">
                                <a class="list-group-item list-group-item-action indicateur" data-id="3" data-type="cercle" data-radius="35" href="#">Nombre de communes</a>
                            </div>
                        </div>
                        <div class="container p-2 m-0">
                            <h4 class="text-dark font-weight-bold p-1">Aplats</h4>
                            <div class="input-group p-0">
                                <a class="col-8 list-group-item list-group-item-action indicateur" data-id="4" data-type="aplat" href="#">Densité de population</a>
                                <select name="anneeDensPop" data-id="4" class="col-4">
                                    <option value="" selected>Année</option>
                                    <option value="d68">1968</option>
                                    <option value="d75">1975</option>
                                    <option value="d82">1982</option>
                                    <option value="d90">1990</option>
                                    <option value="d99">1999</option>
                                    <option value="p06">2006</option>
                                    <option value="p11">2011</option>
                                    <option value="p16">2016</option>
                                </select>
                            </div>
                            <div class="input-group p-0">
                                <a class="col-8 list-group-item list-group-item-action indicateur" data-id="5" data-type="aplat" href="#">Solde naturel</a>
                                <select name="anneeSolde" data-id="5" class="col-4">
                                    <option value="" selected>Période</option>
                                    <option value="6875">1968-1975</option>
                                    <option value="7582">1975-1982</option>
                                    <option value="8290">1982-1990</option>
                                    <option value="9099">1990-1999</option>
                                    <option value="9906">1999-2006</option>
                                    <option value="0611">2006-2011</option>
                                    <option value="1116">2011-2016</option>
                                </select>
                            </div>

                            <div class="input-group pt-1">
                                <label class="col-4 p-0 font-italic">Type de discrétisation</label>
                                <select class="col-8 p-0 custom-select" name="discretisation">
                                    <option value="amplitude">Amplitudes égales</option>
                                    <option value="quantile">Quantiles</option>
                                </select>
                            </div>
                        </div>
                        <div class="container text-center p-2 m-0">
                            <button type="button" class="btn btn-outline-dark w-75 p-1 resetbutton">Remettre à zéro la carte</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="map" class="col-9 p-0"></div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="main.js"></script>
</body>

</html>