function drawAplat(geojson, legende, colors, titre, unite) {

	info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h5 class="text-left font-weight-normal">' + titre + '</h5>' + (props ?
			'<b>' + props.nom_dpt + ' (' + props.code_dpt + ')</b><br />' + props.data + ' ' + props.unite
			: '<p class="text-left font-italic">Survolez un département</p>');
	};

	info.addTo(map);

	function highlightFeature(e) {
		var eventInteraction = e.target;

		eventInteraction.setStyle({
			weight: 5,
			color: '#666',
			dashArray: '',
			fillOpacity: 0.7
		});

		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			eventInteraction.bringToFront();
		}

		info.update(eventInteraction.feature.properties);
	}

	function resetHighlight(e) {
		layer.resetStyle(e.target);
		info.update();
	}

	function onEachFeature(feature, eventInteraction) {
		eventInteraction.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight
		});
	}

	layer = L.geoJSON(geojson, {
		style: function (feature) {
			let fillColor = getColor(feature.properties.data, legende, colors);
			unite = feature.properties.unite;
			return {
				fillColor: fillColor,
				weight: 2,
				opacity: 1,
				color: 'white',
				dashArray: '3',
				fillOpacity: 0.7
			};
		},
		onEachFeature: onEachFeature
	}).bindPopup(popupHTMLAplat).addTo(map);

	legend = L.control({ position: 'bottomright' });
	legend.onAdd = function (map, layer) {
		var div = L.DomUtil.create('div', 'info legend');
		div.innerHTML += '<h6 class="text-left font-italic">En ' + unite + '</h6><br>';
		for (var i = 0; i < colors.length; i++) {
			div.innerHTML +=
				'<i style="background:' + colors[i] + '"></i> ' +
				Math.round(legende[i] * 100) / 100 + " - " + Math.round(legende[i + 1] * 100) / 100 + '<br>';
		}
		return div;
	};
	legend.addTo(map);

}


function drawCercle(geojson, denominateur, titre, unite) {

	var cercleMaxRadius = null;
	var cercleMaxData = null;

	info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h5 class="text-left font-weight-normal">' + titre + '</h5>' + (props ?
			'<b>' + props.nom_dpt + ' (' + props.code_dpt + ')</b><br />' + props.data + ' ' + props.unite
			: '<p class="text-left font-italic">Survolez un département</p>');
	};

	info.addTo(map);

	function highlightFeature(e) {
		var eventInteraction = e.target;

		eventInteraction.setStyle({
			weight: 5,
			color: '#666',
			dashArray: '',
			fillOpacity: 0.7
		});

		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			eventInteraction.bringToFront();
		}

		info.update(eventInteraction.feature.properties);
	}

	function resetHighlight(e) {
		layer.resetStyle(e.target);
		info.update();
	}

	function onEachFeature(feature, eventInteraction) {
		eventInteraction.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight
		});
	}

	function drawCercle(radius, data) {
		var smallRadius = radius / 10,
			smallData = data / 10,
			middleRadius = radius / 5,
			middleData = data / 5,
			intermediateRadius = radius / 2,
			intermediateData = data / 2;

		$('.smallCircle').css({
			'width': smallRadius.toFixed() * 2,
			'height': smallRadius.toFixed() * 2
		});
		
		$('.middleCircle').css({
			'width': middleRadius.toFixed() * 2,
			'height': middleRadius.toFixed() * 2
		});

		$('.intermediateCircle').css({
			'width': intermediateRadius.toFixed() * 2,
			'height': intermediateRadius.toFixed() * 2
		});

		$('.bigCircle').css({
			'width': radius.toFixed() * 2,
			'height': radius.toFixed() * 2
		});

		$(".smallData").html(smallData.toFixed());

		$(".middleData").html(middleData.toFixed());

		$(".intermediateData").html(intermediateData.toFixed());

		$(".bigData").html(data.toFixed());
	}


	layer = L.geoJSON(geojson, {
		pointToLayer: function (geoJsonPoint, latlng) {
			if ((geoJsonPoint.properties.data / denominateur) > cercleMaxRadius) {
				cercleMaxRadius = geoJsonPoint.properties.data / denominateur;
				cercleMaxData = geoJsonPoint.properties.data;
			}
			unite = geoJsonPoint.properties.unite;
			return L.circleMarker(latlng, {
				radius: (geoJsonPoint.properties.data / denominateur)
			});
		},
		onEachFeature: onEachFeature
	}).bindPopup(popupHTMLCercle).addTo(map);

	legend = L.control({ position: 'bottomright' });
	legend.onAdd = function (map) {
		var div = L.DomUtil.create('div', 'info legend');
		div.innerHTML += '<h6 class="text-left font-italic">En ' + unite + '</h6><br>'
			+ '<div class="container-fluid">'
			+ '<div class="row align-items-end p-0">'
			+ '<div class="container col-md-auto p-1">'
			+ '<div class="smallCircle"></div>'
			+ '<div class="text-center smallData"></div>'
			+ '</div>'
			+ '<div class="container col-md-auto p-1">'
			+ '<div class="middleCircle"></div>'
			+ '<div class="text-center middleData"></div>'
			+ '</div>'
			+ '<div class="container col-md-auto p-1">'
			+ '<div class="intermediateCircle"></div>'
			+ '<div class="text-center intermediateData"></div>'
			+ '</div>'
			+ '<div class="container col-md-auto p-1">'
			+ '<div class="bigCircle"></div>'
			+ '<div class="text-center bigData"></div>'
			+ '</div>'
			+ '</div>'
			+ '</div>';
		return div;
	};
	legend.addTo(map);

	drawCercle(cercleMaxRadius, cercleMaxData);

}


function popupHTMLAplat(layer) {
	let html = '<table class="table table-striped m-0">'
		+ '<tr><th>Nom</th><td>' + layer.feature.properties.nom_dpt + '</td></tr>'
		+ '<tr><th>Code</th><td>' + layer.feature.properties.code_dpt + '</td></tr>'
		+ '<tr><th>Densité</th><td>' + layer.feature.properties.data + ' ' + layer.feature.properties.unite + '</td></tr>'
		+ '</table>';

	return html;
}

function popupHTMLCercle(layer) {
	let html = '<table class="table table-striped m-0">'
		+ '<tr><th>Nom</th><td>' + layer.feature.properties.nom_dpt + '</td></tr>'
		+ '<tr><th>Code</th><td>' + layer.feature.properties.code_dpt + '</td></tr>'
		+ '<tr><th>Habitants</th><td>' + layer.feature.properties.data + ' ' + layer.feature.properties.unite + '</td></tr>'
		+ '</table>';

	return html;
}

function getColor(d, legende, colors) {
	let color = null;
	$.each(legende, function (index, value) {
		if (d < parseFloat(value)) {
			color = colors[index - 1];
			return false;
		}
	});
	return color;
}

function getTitre(texte, annee) {
	if (annee != null) {
		var titre = texte + " (" + annee + ")";
	}
	else {
		var titre = texte;
	}
	return titre;
}


var map = L.map('map').setView([46.867173, 2.568093], 6);
var layer = null;
var legend = null;
var unite = null;
var info = null;


var defaultMap = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

var roadMap = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

var satelliteMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});


var baseLayers = {
	"Vue par défaut": defaultMap,
	"Plan": roadMap,
	"Satellite": satelliteMap
};


defaultMap.addTo(map);
L.control.layers(baseLayers, null, { position: 'bottomleft' }).addTo(map);
L.control.scale({ position: 'bottomright', metric: true, imperial: false }).addTo(map);


$(".indicateur").click(function () {
	let id = $(this).data('id');
	switch (id) {
		case 1:
			var anneeTexte = $("select[name='anneePopMuni'] option:selected").text();
			var annee = $("select[name='anneePopMuni'] option:selected").val();
			var titre = getTitre($(this).text(), anneeTexte);
			if (annee === "") {
				alert("Veuillez renseigner une année valide.");
			};
			break;
		case 2:
			anneeTexte = null;
			var titre = getTitre($(this).text(), anneeTexte);
			break;
		case 3:
			anneeTexte = null;
			var titre = getTitre($(this).text(), anneeTexte);
			break;
		case 4:
			var anneeTexte = $("select[name='anneeDensPop'] option:selected").text();
			var annee = $("select[name='anneeDensPop'] option:selected").val();
			var titre = getTitre($(this).text(), anneeTexte);
			if (annee === "") {
				alert("Veuillez renseigner une année valide.");
			};
			break;
		case 5:
			var anneeTexte = $("select[name='anneeSolde'] option:selected").text();
			var annee = $("select[name='anneeSolde'] option:selected").val();
			var titre = getTitre($(this).text(), anneeTexte);
			if (annee === "") {
				alert("Veuillez renseigner une période valide.");
			};
			break;
	}

	let type = $(this).data('type');
	let denominateur = $(this).data('radius');
	let discretisation = $("select[name='discretisation']").val();


	$.ajax({
		url: 'layer.php',
		data: {
			id: id,
			annee: annee
		},
		dataType: 'json',
		success: function (geojson) {
			if (layer) {
				if (legend) {
					map.removeControl(legend);
				}
				if (info) {
					map.removeControl(info);
				}
				map.removeLayer(layer);
			}

			if (type === 'aplat') {
				$.ajax({
					url: 'discretisation.php',
					data: {
						id: id,
						discretisation: discretisation,
						annee: annee
					},
					dataType: 'json',
					success: function (data) {
						drawAplat(geojson, data.legende, data.colors, titre, unite);
					}
				})
			} else if (type === 'cercle') {
				drawCercle(geojson, denominateur, titre, unite);
			}
		}
	})
});

$(".resetbutton").click(function () {
	if (layer) {
		if (legend) {
			map.removeControl(legend);
		}
		if (info) {
			map.removeControl(info);
		}
		map.removeLayer(layer);
	}
	$('select').prop('selectedIndex', 0);
});